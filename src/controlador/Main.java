package controlador;

import modelo.Alumno;
import vista.Vista;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Main {

    public static void main(String[] args) {
        Alumno modelo = null;
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);

    }
}
