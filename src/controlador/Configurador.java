/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import vista.Vista;

public class Configurador {

    private String nombreAplicacion;
    private static Configurador configurador;

    public static Configurador getConfigurador(String nombreAplicacion) {

        Vista vista = new Vista();

        if (configurador == null) {
            configurador = new Configurador(nombreAplicacion);
            vista.mensaje("Creada instancia Configurador");
        } else {

            vista.error("Error : Solo se permite una instancia ");
        }
        return configurador;
    }

    private Configurador(String nombreAplicacion) {
        this.nombreAplicacion = nombreAplicacion;
    }

    public String getNombreAplicacion() {
        return nombreAplicacion;
    }

}
