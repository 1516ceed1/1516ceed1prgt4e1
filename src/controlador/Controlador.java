/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.Alumno;
import modelo.Grupo;
import vista.IVista;
import vista.Vista;
import vista.VistaAlumno;
import vista.VistaGrupo;

/**
 *
 * @author paco
 */
public class Controlador {

    private Alumno alumno;
    private Grupo grupo;

    private Vista vista;
    Configurador configurador; // Patron Singleton

    IVista<Grupo> vistagrupo;  // Vista parametrizada
    IVista<Alumno> vistaaalumno; // Vista parametrizada

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    Controlador(Alumno modelo, Vista vista) {
        this.alumno = modelo;
        this.vista = vista;
        configurar();
        configurar();
        inicio();

    }

    public void inicio() {
        String opcion;

        do {
            vista.menu(configurador.getNombreAplicacion());
            opcion = vista.leerString();
            switch (opcion) {
                case "1":
                    grupo();
                    break;
                case "2":
                    alumno();
                    break;
            }

        } while (!opcion.equals("0"));

    }

    private void grupo() {
        vistagrupo = new VistaGrupo();
        grupo = vistagrupo.obtener();
    }

    private void alumno() {

        IVista<Alumno> vistaaalumno = new VistaAlumno();

        if (grupo != null) {
            alumno = vistaaalumno.obtener();
            alumno.setGrupo(grupo);
            vistaaalumno.mostrar(alumno);
        } else {
            vista.error("Error: No existe grupo;");
        }

    }

    private void configurar() {
        configurador = Configurador.getConfigurador("PRACTICA CLASES");

    }

}
