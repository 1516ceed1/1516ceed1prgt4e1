/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Alumno;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class VistaAlumno implements IVista<Alumno> {

    public Alumno obtener() {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        Alumno a = new Alumno();
        String id = "";
        String nombre = "";

        System.out.println("OBTENER ALUMNO.");

        try {

            System.out.print("Id: ");
            id = br.readLine();

            System.out.print("Nombre: ");
            nombre = br.readLine();

        } catch (IOException ex) {
            Logger.getLogger(VistaAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }

        a.setId(id);
        a.setNombre(nombre);
        return a;
    }

    public void mostrar(Alumno a) {

        VistaGrupo vg = new VistaGrupo();

        System.out.println("************P");
        System.out.println("MOSTRAR ALUMNO.");
        System.out.println("*************");
        System.out.println("Id: " + a.getId());
        System.out.println("Nombre: " + a.getNombre());
        System.out.println("El alumno pertenece al siguiente grupo: ");
        vg.mostrar(a.getGrupo());

    }

}
