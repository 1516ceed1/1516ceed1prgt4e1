/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Alumno;
import modelo.Grupo;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class VistaGrupo implements IVista<Grupo> {

    public Grupo obtener() {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        Grupo g = new Grupo();
        String id = "";
        String nombre = "";

        System.out.println("OBTENER GRUPO.");

        try {

            System.out.print("Id: ");
            id = br.readLine();

            System.out.print("Nombre: ");
            nombre = br.readLine();

        } catch (IOException ex) {
            Logger.getLogger(VistaGrupo.class.getName()).log(Level.SEVERE, null, ex);
        }

        g.setId(id);
        g.setNombre(nombre);

        return g;
    }

    public void mostrar(Grupo g) {
        System.out.println("***********");
        System.out.println("MOSTRAR GRUPO");
        System.out.println("***********");
        System.out.println("Id: " + g.getId());
        System.out.println("Nombre: " + g.getNombre());
    }

}
