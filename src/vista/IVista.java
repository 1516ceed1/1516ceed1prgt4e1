package vista;

public interface IVista<T> {

    public void mostrar(T t);

    public T obtener();

}
