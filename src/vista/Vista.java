/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Configurador;
import java.util.Scanner;

/**
 *
 * @author paco
 */
public class Vista {

    public void menu(String titulo) {

        System.out.println("** MENU ** ");
        System.out.println(titulo);
        System.out.println("0.Salir");
        System.out.println("1.Grupo");
        System.out.println("2.Alumno");
        System.out.print("Opcion: ");
    }

    public String leerString() {
        String s;
        Scanner sc = new Scanner(System.in);
        s = sc.nextLine();
        return s;

    }

    public void error(String error) {
        System.err.println(error);
    }

    public void mensaje(String men) {
        System.out.println(men);
    }

}
